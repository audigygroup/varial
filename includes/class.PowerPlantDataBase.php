<?php

/**
 * PowerPlantPDO
 * Creates database connection to PowerPlant for use in features
 *
 * @package   Varial_Plugin
 * @author    David Wright <dwright@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 */
class PowerPlantDataBase
{
    const POWER_PLANT_DB_USER = 'powerplant';
    const POWER_PLANT_DB_PASSWORD = '!@RussWilson12';
    const POWER_PLANT_DB_NAME = 'powerplant';
    const POWER_PLANT_DB_HOST = '40c156ce14b5782f95d7ea232d662d8a561f6235.rackspaceclouddb.com';

    protected $ppwpdb;

    private function __construct()
    {
        $this->ppwpdb = new wpdb(
            self::POWER_PLANT_DB_USER,
            self::POWER_PLANT_DB_PASSWORD,
            self::POWER_PLANT_DB_NAME,
            self::POWER_PLANT_DB_HOST
        );

        $this->ppwpdb->show_errors();
    }

    public static function getInstance()
    {
        return new PowerPlantDataBase();
    }

    public function getResults( $query )
    {
        if (empty( $query )) {
            return false;
        }
        return $this->ppwpdb->get_results( $query );
    }
}