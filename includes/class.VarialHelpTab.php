<?php
/**
 * Varial_Plugin.
 *
 * @package   Varial_Plugin
 * @author    Nathan Killen <nkillen@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 */

/**
 * VarialHelpTab class. This Class Controls the Help Tab
 * on the admin side of the WordPress site.
 *
 */
class VarialHelpTab{

	private function __construct() {
		add_action( 'in_admin_header', array( $this, 'add_tabs' ), 20 );
    }

    public static function getInstance() {
        return new VarialHelpTab();
    }


	public $tabs = array(
		'varialControls' => array(
			'title'    => 'Varial Controls',
			'content'  => 'To switch features on and off, click the Varial Controls menu item. Check the boxes of features you want to turn on and save changes. Note that if you turn off Google Analytics, CallRail will be turned off too. CallRail is dependant on Google Analytics.'
		),
		'varialGA' => array(
		 	'title'   => 'Google Analytics',
			'content' => 'When activated, the GA code can be added under <strong>Varial Controls > Google Analytics</strong>. The GA code must be retrieved from the correct GA account. A Google Chrome extention called "Tag Assistant (by Google)" can be used to make sure the code is working right. (It often claims the code is not on the first load of the page.)'
		),
		'varialCallRail' => array(
		 	'title'   => 'CallRail',
			'content' => 'When activated, the script can be added under <strong>Varial Controls > CallRail</strong>.'
		),
		'varialFAQ' => array(
		 	'title'   => 'FAQ Feature',
			'content' => 'When activated, these are managed under the <strong>FAQ</strong> section to the left. To reposition an item, simply drag and drop the row by "clicking and holding" it anywhere (outside of the links and form controls) and moving it to its new position. To see what category you are in use the "Filter by Category" dropdown. These FAQs are designed to be used with a shortcode. The shortcode is <strong>[custom_faq]</strong> if you want to break them out by category you must use <strong>[custom_faq faq_cat="category-slug-goes-here]</strong>'
		),
		'varialStreamer' => array(
		 	'title'   => 'Streamer Shortcode',
			'content' => 'Activating this allows the use of a shortcode to display the shared streamer hearing aid accessory page. The page "Bluetooth Hearing Aid Accessories" must be created featuring the shortcode <strong>[streamer_page]</strong>. If the site has a "Made for iPhone" page, a link can be added like <strong>[streamer_page made_for_iphone_url="/hearing-aids/hearing-aid-styles/agxsp-hearing-aid"]</strong>. Note that the url should be the current site\'s own path to the "Made for iPhone" page, and should begin with a / in place of the site address.'
		)
	);

	public function add_tabs() {
		foreach ( $this->tabs as $id => $data ) {
			get_current_screen()->add_help_tab( array(
				'id'       => $id,
				'title'    => __( $data['title']), 
				'content'  => '<p>Varial Plugin Help</p>',
				'callback' => array( $this, 'prepare' )
			) );
		}
	}

	public function prepare( $screen, $tab ) {
    	printf( 
		 	'<p>%s</p>'
			,__( 
	    		$tab['callback'][0]->tabs[ $tab['id'] ]['content'], 
				'dmb_textdomain' 
			)
		);
	}
}