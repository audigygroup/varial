<?php
/**
 * Varial_Plugin.
 *
 * @package   Varial_Plugin
 * @author    Nathan Killen <nkillen@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 */

/**
 * FeatureAdminSwitch class. This Class Controls the Varial Controls
 * on the admin side of the WordPress site.
 *
 */
class FeatureAdminSwitch {
	private function __construct() {
		$featureEnabled = get_option( 'features_enabled' );
		add_action( 'admin_init', array( $this, 'varialFeatureInit' ) );
	}

	public static function getInstance() {
		return new featureAdminSwitch();
	}

	public function varialFeatureInit() {


		add_settings_section(
			'features_section',
			'',
			array( $this, 'featuresSectionCallback' ),
			'varial-controls'
		);

		add_settings_field(
			'features_enabled',
			'Enable Features',
			array( $this, 'featuresSettingCallback' ),
			'varial-controls',
			'features_section'
		);

		register_setting( 'varial-controls', 'features_enabled', array( $this, 'featuresEnabling' ) );
	}

	public function featuresSectionCallback() {
	}

	public function featuresSettingCallback() {
		$options = get_option( 'features_enabled' );
		$options = json_decode( $options );
		$checked = 'checked="checked"';

		$isGaEnabled       = ( empty ( $options->ga ) ) ? '' : $checked;
		$isCallrailEnabled = ( ! empty( $options->ga ) && ! empty ( $options->callrail ) ) ? $checked : '';
		$isFaqEnabled      = ( empty ( $options->faq ) ) ? '' : $checked;
		$isStreamerShortCodeEnabled = (empty( $options->streamer_shortcode)) ? '' : $checked;


		echo <<<HTML
        
        <input name="featureGaEnabled" id="ga-enabled" type="checkbox" value="1" {$isGaEnabled}>Check this box to enable Google Analytics on this site. <em><strong>Note:</strong> If you Turn off this feature CallRail will be also turned off.</em><br/><br/>
        <input name="featureCallrailEnabled" id="callrail_enabled" type="checkbox" value="1" {$isCallrailEnabled}>Check this box to enable <a href="http://www.callrail.com/" target="_blank">CallRail</a> on this site.<br/><br/>
        <input name="featureFaqEnabled" id="" type="checkbox" value="1" {$isFaqEnabled}>Check this box to enable FAQ Feature on this site.<br/><br/>
        <input name="featureStreamerShortCodeEnabled" type="checkbox" value="1" {$isStreamerShortCodeEnabled}>Check this box to enable the Streamer Shortcode.
HTML;

	}

	public function featuresEnabling() {

		$varialGA       = isset( $_POST['featureGaEnabled'] ) ? $_POST['featureGaEnabled'] : '0';
		$varialCallrail = isset( $_POST['featureCallrailEnabled'] ) ? $_POST['featureCallrailEnabled'] : '0';
		$varialFaq      = isset( $_POST['featureFaqEnabled'] ) ? $_POST['featureFaqEnabled'] : '0';
		$varialStreamerShortCode = isset( $_POST['featureStreamerShortCodeEnabled'] ) ? $_POST['featureStreamerShortCodeEnabled'] : '0';


		$featureEnabled = array(
			'ga'       => $varialGA,
			'callrail' => $varialCallrail,
			'faq'      => $varialFaq,
			'streamer_shortcode' => $varialStreamerShortCode
		);

		return json_encode( $featureEnabled );

	}

}