<?php

/**
 * @TODO document
 *
 * Admin:
 * - 
 * - 
 *
 * Public:
 * - 
 */

include_once(plugin_dir_path( __FILE__ ) . 'includes/class.FaqPostType.php' );
require_once( plugin_dir_path( __FILE__ ) . 'public/class.FaqPlugin.php' );

add_action( 'plugins_loaded', array( 'FaqPlugin', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class.FaqPluginAdmin.php' );
	add_action( 'plugins_loaded', array( 'FaqPluginAdmin', 'get_instance' ) );
}

