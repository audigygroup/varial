<?php

require_once WP_PLUGIN_DIR . '/varial/features/faq/admin/includes/class.FaqSortable.php';
require_once WP_PLUGIN_DIR . '/varial/features/faq/admin/includes/class.FaqFilter.php';

class FaqPluginAdmin {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	protected static $instance = null;



	private function __construct() {
		FaqPostType::getInstance();
		FaqSortable::getInstance();
		FaqFilter::getInstance();
	}
	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}	
}