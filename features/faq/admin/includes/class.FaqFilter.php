<?php

class FaqFilter{
	private function __construct() {
        add_action( 'restrict_manage_posts', array( $this, 'my_restrict_manage_posts') );
    }
    public static function getInstance() {
        return new FaqFilter();
    }

	public static function my_restrict_manage_posts() {
	    global $typenow;
	    $taxonomy = 'faq';
	    if( $typenow != "page" && $typenow != "post" ){
	       $filters = get_object_taxonomies($taxonomy);

	        foreach ($filters as $tax_slug) {
	            $tax_obj = get_taxonomy($tax_slug);
	            $tax_name = $tax_obj->labels->name;
	            $terms = get_terms($tax_slug);

	            echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
	            echo "<option value=''>Filter By Category</option>";
	            foreach ($terms as $term) { 

	                $label = (isset($_GET[$tax_slug])) ? $_GET[$tax_slug] : '';
	                echo '<option value='. $term->slug, $label == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>';
	            }
	            echo "</select>";
	        }
	    }
	}
}