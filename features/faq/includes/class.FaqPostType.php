<?php
class FaqPostType{
    private function __construct() {
        add_action( 'init', array( $this, 'create_faq_post_type') );
        add_action( 'init', array( $this, 'create_faq_taxonomy') );
    }

    public static function getInstance() {
        return new FaqPostType();
    }

    public static function create_faq_post_type(){
        $labels = array(
            'name'               => _x( 'FAQ', 'FAQ', 'varial' ),
            'singular_name'      => _x( 'FAQ', 'FAQ', 'varial' ),
            'menu_name'          => _x( 'FAQ', 'admin menu', 'varial' ),
            'name_admin_bar'     => _x( 'FAQ', 'add new on admin bar', 'varial' ),
            'add_new'            => _x( 'Add New', 'FAQ', 'varial' ),
            'add_new_item'       => __( 'Add New FAQ', 'varial' ),
            'new_item'           => __( 'New FAQ', 'varial' ),
            'edit_item'          => __( 'Edit FAQ', 'varial' ),
            'view_item'          => __( 'View FAQ', 'varial' ),
            'all_items'          => __( 'All FAQ', 'varial' ),
            'search_items'       => __( 'Search FAQ', 'varial' ),
            'parent_item_colon'  => __( 'Parent FAQ:', 'varial' ),
            'not_found'          => __( 'No FAQ found.', 'varial' ),
            'not_found_in_trash' => __( 'No FAQ found in Trash.', 'varial' )
        );

        $args = array(
            'labels'             => $labels,
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'rewrite'            => false,
            'capability_type'    => 'post',
            'has_archive'        => true,
            'hierarchical'       => true,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor' ),
        );
        register_post_type( 'faq', $args );
    }

    public static function create_faq_taxonomy(){
        $labels = array(
        	'name' => 'FAQ Categories',
        	'singular_name' => 'Categories', 
        	'search_items' => 'Search Types', 
        	'all_items' => 'All Types', 
        	'parent_item' => 'Parent Type', 
        	'parent_item_colon' => 'Parent Type:',
        	'edit_item' => 'Edit Type', 
        	'update_item' => 'Update Type', 
        	'add_new_item' => 'Add New Type', 
        	'new_item_name' => 'New Type Name', 
        	'menu_name' => 'FAQ Categories'
        );

        $args = array(
        	'labels' => $labels, 
        	'show_ui'           =>  true,
            'hierarchical'      =>  true,
            'show_admin_column' =>  true,
            'query_var'         =>  true,
            'rewrite'           =>  true
        );
        register_taxonomy( 'faq_cat', 'faq', $args );
    }
}