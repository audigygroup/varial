<div class="accordion">
	<div class="container-faq">
		<?php if($faq_catagories):?>
			<?php foreach($faq_catagories as $post):?>
				<div class="faq-row">
					<div class="column-content">
						<h3><span class="expand-box plus"></span> <span class="title"><?php echo $post->post_title;?></span></h3>
						<div class="content">
							<p><?php echo $post->post_content;?></p>
						</div>
					</div>
				</div>	
			<? endforeach; ?>
		<?php endif;?>
	</div>
</div>