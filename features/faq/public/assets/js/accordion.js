(function($) {
    
  var allContent = $('.container-faq > .faq-row .content').hide();

  $('.container-faq > .faq-row .expand-box').click(function() {
    allContent.slideUp( function(){
    	$(this).parent().find('.expand-box').addClass('plus').removeClass('minus');
    });

    if( $(this).parent().find('.expand-box').hasClass('plus') ){
      $(this).parent().next().slideDown( function(){
        $(this).parent().find('.expand-box').addClass('minus').removeClass('plus');
      });
    }
    return false;
  });

})(jQuery);