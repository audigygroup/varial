<?php
class FaqPlugin {
		/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;


	private function __construct() {
		add_shortcode( 'custom_faq' , array( $this, 'faq_page' ) );
		FaqPostType::getInstance();
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function faq_page($atts) {
		$faq_cat = isset($atts["faq_cat"]) ? $atts["faq_cat"] : '';

		global $wpdb;

		$cat_rs = $wpdb->get_results(
			"SELECT term_taxonomy_id
			FROM wp_term_taxonomy 
			INNER JOIN wp_terms ON wp_terms.term_id = wp_term_taxonomy.term_id 
			WHERE  wp_terms.slug = '".$faq_cat."' AND wp_term_taxonomy.taxonomy = 'faq_cat' ",
			ARRAY_N
		);
			
		$term_taxonomy_id =	$cat_rs[0][0];

		$query = "SELECT * 
			FROM $wpdb->posts INNER JOIN wp_term_relationships ON (wp_posts.ID = wp_term_relationships.object_id) 
			WHERE 1=1 
			AND ( wp_term_relationships.term_taxonomy_id 
				IN ($term_taxonomy_id ) )
			AND post_type = 'faq' 
			AND (post_status = 'publish' 
				OR wp_posts.post_status = 'private') 
			GROUP BY wp_posts.ID 
			ORDER BY wp_posts.menu_order 
			ASC LIMIT 0, 10
			";

		$faq_catagories = $wpdb->get_results($query);
		ob_start();

		include('views/faq-accordion.php');
		$output_buffer = ob_get_contents();

		ob_end_clean();
		return $output_buffer;
	}
}