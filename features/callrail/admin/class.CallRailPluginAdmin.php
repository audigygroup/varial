<?php

class CallRailPluginAdmin {

	/**
	 * Instance of this class.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	protected static $instance = null;

	/**
	 *
	 */
	private function __construct() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		/*
		 * Call $plugin_slug from public plugin class.
		 *
		 */

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );
		add_action( 'admin_init', array( $this, 'callrail_options_init' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		/*
		 * Add a settings page for this plugin to the Settings menu.
		 *
		 * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
		 *
		 *        Administration Menus: http://codex.wordpress.org/Administration_Menus
		 *
		 * @TODO:
		 *
		 * - Change 'Page Title' to the title of your plugin admin page
		 * - Change 'Menu Text' to the text for menu item for the plugin settings page
		 * - Change 'manage_options' to the capability you see fit
		 *   For reference: http://codex.wordpress.org/Roles_and_Capabilities
		 *
		 */

		//Removed to add to one menu
		// $this->plugin_screen_hook_suffix = add_options_page(
		// 	__( 'CallRail', 'callrail' ),
		// 	__( 'CallRail', 'callrail' ),
		// 	'manage_options',
		// 	'callrail',
		// 	array( $this, 'display_settings_page' )
		// );

		add_submenu_page('varial-controls', 'CallRail', 'CallRail', 'manage_options' , 'callrail', array($this, 'display_settings_page'));
	}

	public function display_settings_page() {
		include_once('views/admin-callrail.php');
	}

	public function callrail_options_init() {

		// Add the section to reading settings so we can add our
		// fields to it
		add_settings_section(
			'callrail_setting_section',
			'',
			array( $this, 'callrail_setting_section_callback_function' ),
			'callrail'
		);

		// add_settings_field(
		// 	'callrail_enable',
		// 	'Enable CallRail',
		// 	array( $this, 'callrail_enable_callback_function' ),
		// 	'callrail',
		// 	'callrail_setting_section'
		// );

		add_settings_field(
			'callrail_javascript',
			'Call Rail Script',
			array( $this, 'callrail_javascript_callback_function' ),
			'callrail',
			'callrail_setting_section'
		);

		// Register our setting so that $_POST handling is done for us and
		// register_setting( 'callrail', 'callrail_enable' );
		register_setting( 'callrail', 'callrail_javascript' );
	}

	public function callrail_setting_section_callback_function() {

	}

	public function callrail_javascript_callback_function() {

		$callRailJS = get_option('callrail_javascript');
		echo <<<HTML
        <textarea cols="70" rows="4" id="callrail_javascript" name="callrail_javascript">{$callRailJS}</textarea>
        <p>Enter the entire <a href="http://www.callrail.com/">CallRail</a> script, including the &lt;script&gt; tags.</p>
HTML;
	}

// 	public function callrail_enable_callback_function() {

// 		// Get enabled flag
// 		$isCalLRailEnabled = checked( 1, get_option( 'callrail_enable' ), false );

// 		echo <<<HTML
//         <input name="callrail_enable" id="callrail_enable" type="checkbox" value="1" {$isCalLRailEnabled} /> Check this box to enable <a href="http://www.callrail.com/">CallRail</a> for this site, then copy and paste the CallRail javascript below.
// HTML;
// 	}

}