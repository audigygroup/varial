<?php

/**
 * @TODO document
 *
 * Admin:
 * - Enable toggling callrail on/off
 * - Enable adding callrail javascript
 *
 * Public:
 * - Insert CallRail JavaScript with members code injected
 */

require_once( plugin_dir_path( __FILE__ ) . 'public/class.CallRailPlugin.php' );
add_action( 'plugins_loaded', array( 'CallRailPlugin', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class.CallRailPluginAdmin.php' );
	add_action( 'plugins_loaded', array( 'CallRailPluginAdmin', 'get_instance' ) );
}
