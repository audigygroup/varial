<?php

/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 8/25/14
 * Time: 12:53 PM
 */
class CallRailPlugin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	private function __construct() {
		add_action( 'wp_footer', array( $this, 'action_add_callrail_script' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	public function action_add_callrail_script() {
		$callRailScript = get_option('callrail_javascript');
		include_once('views/callrail.php');
	}
}