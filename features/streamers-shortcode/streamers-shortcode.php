<?php

require_once( plugin_dir_path( __FILE__ ) . 'public/class.StreamersShortCodePublic.php' );

add_action( 'plugins_loaded', array( 'StreamersShortCodePublic', 'getInstance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * The code below is intended to to give the lightest footprint possible.
 */
if (is_admin()) {
    require_once( plugin_dir_path( __FILE__ ) . 'admin/class.StreamersShortCodeAdmin.php' );
    add_action( 'plugins_loaded', array( 'StreamersShortCodeAdmin', 'getInstance' ) );
}

