<?php
/**
 * StreamersShortCode
 * This short code is used to display a common page from PowerPlant
 */
class StreamersShortCode
{
    private function __construct()
    {
        add_shortcode( 'streamer_page', array( $this, 'add_shortcode' ) );
    }

    public static function getInstance()
    {
        return new StreamersShortCode();
    }

    function add_shortcode( $atts )
    {
        $a = shortcode_atts( array(
            'made_for_iphone_url' => '',
        ), $atts );

        ob_start();

        include WP_PLUGIN_DIR . '/varial/features/streamers-shortcode/public/views/blue-tooth-hearing-accessories.html';
        $output_buffer = ob_get_contents();

        $str = 'Made-for-iPhone&#174; Hearing Aids';
        if (!empty($a['made_for_iphone_url'])) {
            $output_buffer = str_replace(
                $str,
                sprintf('<a href="%s">%s</a>', $a['made_for_iphone_url'], $str),
                $output_buffer
            );
        }

        ob_end_clean();

        return $output_buffer;
    }
}