<?php

/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 8/25/14
 * Time: 12:53 PM
 */
class GoogleAnalyticsPlugin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;


	private function __construct() {
		
		add_action( 'wp_head', array( $this, 'action_universal_google_analytics' ) );
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Render the Universal GA javascript injected with the GA code,
	 * Only if there is a GA code
	 */
	public function action_universal_google_analytics()
	{
		$ga_property_id = get_option('googleanalytics_property_id');

		// Don't load the google javascript if a GA code is not set
		if (empty($ga_property_id)) { return; }

		include_once('views/universal-ga.php');
	}
} 