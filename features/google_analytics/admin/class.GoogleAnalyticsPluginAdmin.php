<?php

class GoogleAnalyticsPluginAdmin
{

    /**
     * Instance of this class.
     *
     * @since 1.0.0
     *
     * @var object
     */
    protected static $instance = null;

    /**
     *
     */
    private function __construct()
    {
        // Add the options page and menu item.
        add_action('admin_menu', array($this, 'add_plugin_admin_menu'));
        add_action('admin_init', array($this, 'googleanalytics_options_init'));
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance()
    {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu()
    {
        // $this->plugin_screen_hook_suffix = add_options_page(
        //     __('Google Analytics', 'googleanalytics'),
        //     __('Google Analytics', 'googleanalytics'),
        //     'manage_options',
        //     'googleanalytics',
        //     array($this, 'display_ga_code_plugin_admin_page')
        // );
        add_submenu_page('varial-controls', 'Google Analytics', 'Google Analytics', 'manage_options' , 'googleanalytics', array($this, 'display_ga_code_plugin_admin_page'));
    }

    public function googleanalytics_options_init()
    {

        // Add the section to reading settings so we can add our
        // fields to it
        add_settings_section(
            'googleanalytics_setting_section',
            '',
            array($this, 'googleanalytics_setting_section_callback_function'),
            'googleanalytics'
        );

        // Add the field with the names and function to use for our new
        add_settings_field(
            'googleanalytics_property_id',
            'Analytics Property ID:',
            array($this, 'googleanalytics_setting_callback_function'),
            'googleanalytics',
            'googleanalytics_setting_section'
        );

        // Register our setting so that $_POST handling is done for us and
        // our callback function just has to echo the <input>
        register_setting('googleanalytics', 'googleanalytics_property_id');

    }

    /**
     * required callback method for section definition, currently not being used
     */
    public function googleanalytics_setting_section_callback_function()
    {
        return;
    }

    public function display_ga_code_plugin_admin_page()
    {
        include_once('views/admin-google-analytics.php');
    }

    public function googleanalytics_setting_callback_function()
    {
        $googleanalytics_property_id = get_option('googleanalytics_property_id');

        echo <<<HTML
            <input type="text" id="googleanalytics_property_id" name="googleanalytics_property_id" value="{$googleanalytics_property_id}">
HTML;
    }
}