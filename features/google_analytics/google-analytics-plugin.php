<?php
/**
 * Google Analytics Plugin
 *
 * @package   Google_Analytics_Plugin
 * @author    David Wright <dwright@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 */

/**
 * Main file for Google Analytics Plugin
 *
 * Defines public and admin configuration files
 *
 * Responsible for configuring the Google Analytics Public and Admin interfaces and functionality.
 * CSS and JS will be minified into main plugin css and js
 *
 * Admin:
 * - Enable managing GA code via WP Admin
 * - Toggle between legacy and universal GA code, swaps include JavaScript block
 * - Toggle GA Pinger script used with CallRail
 *
 * Public:
 * - Insert GA JavaScript with members GA code injected based on legacy/universal option
 * - Optionally insert GA Pinger script
 *
 * @package Google_Analytics_Plugin
 * @author  David Wright <dwright@audigygroup.com>
 */


/*----------------------------------------------------------------------------*
 * Public Functionality
 *----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'public/class.GoogleAnalyticsPlugin.php' );
add_action( 'plugins_loaded', array( 'GoogleAnalyticsPlugin', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class.GoogleAnalyticsPluginAdmin.php' );
	add_action( 'plugins_loaded', array( 'GoogleAnalyticsPluginAdmin', 'get_instance' ) );
}
