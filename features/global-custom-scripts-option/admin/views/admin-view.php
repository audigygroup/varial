<?php
/**
 * Represents the view for the administration dashboard.
 *
 * This includes the header, options, and other information that should provide
 * The User Interface to the end user.
 *
 * @package   Plugin_Name
 * @author    Your Name <email@example.com>
 * @license   GPL-2.0+
 * @link      http://example.com
 * @copyright 2014 Your Name or Company Name
 */
?>
<div class="wrap">
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<form method="POST" action="options.php">
		<?php
        settings_fields( 'global_custom_script_option_section', 'global_custom_script_option_section_gtm' );

		do_settings_sections( 'global_custom_script_option' );

		submit_button();
		?>
	</form>
</div>
