<?php

class GlobalCustomScriptsOptionAdmin
{
    /**
     * Instance of this class.
     *
     * @since 1.0.0
     *
     * @var object
     */
    protected static $instance = null;

    private function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );
        add_action( 'admin_init', array( $this, 'options_init' ) );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    /**
     * Register the administration menu for this plugin into the WordPress Dashboard menu.
     *
     * @since    1.0.0
     */
    public function add_plugin_admin_menu()
    {
        add_submenu_page(
            'varial-controls', //parent_slug
            'Global Custom Scripts', //page_title
            'Global Custom Scripts', //menu_title
            'manage_options', //capability
            'global_custom_script_option', //menu_slug
            array( $this, 'add_submenu_page_callback' ) //callback_function
        );
    }

    public function add_submenu_page_callback()
    {
        include_once( 'views/admin-view.php' );
    }

    public function options_init()
    {

        //register_setting( $option_group, $option_name, $sanitize_callback = '' )
        register_setting( 'global_custom_script_option_section', 'global_custom_script_option_setting');
        register_setting( 'global_custom_script_option_section', 'global_custom_script_option_setting_gtm');

        // add_settings_section($id, $title, $callback, $page)
        add_settings_section(
            'global_custom_script_option_section', //section_id
            'Add complete blocks of javascript, including the tags you want to appear on every page.', //title
            array( $this, 'setting_section_callback_function' ), //callback
            'global_custom_script_option' //page
        );

        // add_settings_field($id, $title, $callback, $page, $section = 'default', $args = array())
        add_settings_field(
            'global_custom_script_option_setting', //setting_id
            'Custom Global Scripts', // setting title
            array( $this, 'script_block_callback_function' ), //callback
            'global_custom_script_option', //page
            'global_custom_script_option_section' //section
        );

        add_settings_field(
            'global_custom_script_option_setting_gtm', //setting_id
            'Google Tag Manager Script', // setting title
            array( $this, 'script_block_callback_function_gtm' ), //callback
            'global_custom_script_option', //page
            'global_custom_script_option_section' //section
        );
    }

    public function setting_section_callback_function()
    {
        return;
    }

    public function script_block_callback_function()
    {
        echo '<textarea cols="70" rows="10" id="global_custom_script_option_setting" name="global_custom_script_option_setting">' . get_option( 'global_custom_script_option_setting' ) . '</textarea>';
    }

    public function script_block_callback_function_gtm()
    {
        echo '<textarea cols="70" rows="10" id="global_custom_script_option_setting_gtm" name="global_custom_script_option_setting_gtm">' . get_option( 'global_custom_script_option_setting_gtm' ) . '</textarea>';
    }
}