<?php

/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 8/25/14
 * Time: 12:53 PM
 */
class GlobalCustomScriptsOptionPublic
{
    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    private function __construct()
    {
        // add scripts just above closing body tag
        add_action( 'wp_footer', array( $this, 'action' ), 100 );
        add_action( 'body_class', array( $this, 'actionGTM' ), 10000 );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {

        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function action()
    {
        $option = get_option( 'global_custom_script_option_setting' );
        include_once( 'views/public-view.php' );
    }
    public function actionGTM()
    {
        $classes[] = '">' . get_option( 'global_custom_script_option_setting_gtm' ) . '<br style="display:none;';
        return $classes;
    }
}