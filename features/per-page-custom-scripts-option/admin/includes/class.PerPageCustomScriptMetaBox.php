<?php

/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 4/3/15
 * Time: 10:20 AM
 */
class PerPageCustomScriptMetaBox
{
    public function __construct()
    {
        add_action( 'add_meta_boxes', array( $this, 'add_meta_box' ) );
        add_action( 'save_post', array( $this, 'save' ) );
    }

    /**
     * Adds the meta box container.
     */
    public function add_meta_box( $post_type )
    {
        $post_types = array( 'post', 'page' );     //limit meta box to certain post types
        if (in_array( $post_type, $post_types )) {
            add_meta_box(
                'per_page_custom_script_meta_box_name'
                , __( 'Custom Script For This Page (i.e., page specific reviews/testimonials javascript)', 'per_page_custom_script_textdomain' )
                , array( $this, 'render_meta_box_content' )
                , $post_type
                , 'advanced'
                , 'high'
            );
        }
    }

    /**
     * Save the meta when the post is saved.
     *
     * @param int $post_id The ID of the post being saved.
     */
    public function save( $post_id )
    {
        /*
         * We need to verify this came from the our screen and with proper authorization,
         * because save_post can be triggered at other times.
         */

        // Check if our nonce is set.
        if ( ! isset( $_POST['per_page_custom_script_inner_custom_box_nonce'] )) {
            return $post_id;
        }

        $nonce = $_POST['per_page_custom_script_inner_custom_box_nonce'];

        // Verify that the nonce is valid.
        if ( ! wp_verify_nonce( $nonce, 'per_page_custom_script_inner_custom_box' )) {
            return $post_id;
        }

        // If this is an autosave, our form has not been submitted,
        //     so we don't want to do anything.
        if (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) {
            return $post_id;
        }

        // Check the user's permissions.
        if ('page' == $_POST['post_type']) {

            if ( ! current_user_can( 'edit_page', $post_id )) {
                return $post_id;
            }

        } else {

            if ( ! current_user_can( 'edit_post', $post_id )) {
                return $post_id;
            }
        }

        /* OK, its safe for us to save the data now. */

        // Sanitize the user input.
        //$mydata = sanitize_text_field( $_POST['per_page_custom_scripts'] );

        //Intentionally not sanitizing to retain script tags
        $data = $_POST['per_page_custom_scripts'];

        // Update the meta field.
        update_post_meta( $post_id, '_per_page_custom_scripts', $data );
    }


    /**
     * Render Meta Box content.
     *
     * @param WP_Post $post The post object.
     */
    public function render_meta_box_content( $post )
    {
        // Add an nonce field so we can check for it later.
        wp_nonce_field( 'per_page_custom_script_inner_custom_box', 'per_page_custom_script_inner_custom_box_nonce' );

        // Use get_post_meta to retrieve an existing value from the database.
        $value = get_post_meta( $post->ID, '_per_page_custom_scripts', true );

        echo '<div>Add reviews/testimonials Javascript blocks here, then add the target divs/ids in the content above.</div>';
        echo <<<HTML
            <textarea id="per_page_custom_scripts" cols="50" rows="8" name="per_page_custom_scripts">{$value}</textarea>
HTML;

    }
}