<?php

/**
 * Class PerPageCustomScriptsOptionAdmin
 *
 * Add metabox to pages to enable adding custom script blocks.
 * Intended to be used for things such as adding specific testimonial/review widgets per page
 */
class PerPageCustomScriptsOptionAdmin
{
    /**
     * Instance of this class.
     *
     * @since 1.0.0
     *
     * @var object
     */
    protected static $instance = null;

    private function __construct()
    {
        if ( is_admin() ) {
            add_action( 'load-post.php', array($this, 'CallPerPageCustomScriptMetaBox') );
            add_action( 'load-post-new.php', array($this, 'CallPerPageCustomScriptMetaBox') );
        }
    }

    public function CallPerPageCustomScriptMetaBox() {
        new PerPageCustomScriptMetaBox();
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }
}