<?php

/**
 * global-custom-scripts-option
 * Add theme option to enable adding code into wp_head or wp_footer.
 * For use for things like, callrail script, remarketing script, etc.
 * Scripts added here will be displayed on every page
 *
 * Use Per Page Script Option to add scripts to specific pages like, review us links with specific review ids
 *
 */


require_once( plugin_dir_path( __FILE__ ) . 'public/class.PerPageCustomScriptsOptionPublic.php' );
add_action( 'plugins_loaded', array( 'PerPageCustomScriptsOptionPublic', 'getInstance' ) );

/*----------------------------------------------------------------------------*
 * Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin() && ( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX ) ) {
    require_once( plugin_dir_path( __FILE__ ) . 'admin/includes/class.PerPageCustomScriptMetaBox.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class.PerPageCustomScriptsOptionAdmin.php' );
	add_action( 'plugins_loaded', array( 'PerPageCustomScriptsOptionAdmin', 'getInstance' ) );
}
