<?php

/**
 * PerPageCustomScriptsOptionPublic
 * User: dwright
 * Date: 8/25/14
 * Time: 12:53 PM
 *
 * Output custom script if it is defined for current post, intended to be used for
 * page specific 3rd party reviews or testimonials
 */
class PerPageCustomScriptsOptionPublic
{
    /**
     * Instance of this class.
     *
     * @since    1.0.0
     *
     * @var      object
     */
    protected static $instance = null;

    private function __construct()
    {
        // add scripts just above closing body tag
        add_action( 'wp_footer', array( $this, 'action' ), 100 );
    }

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function getInstance()
    {
        // If the single instance hasn't been set, set it now.
        if (null == self::$instance) {
            self::$instance = new self;
        }

        return self::$instance;
    }

    public function action()
    {
        global $post;
        $perPageScript = get_post_meta( $post->ID, '_per_page_custom_scripts', true );
        include_once( 'views/public-view.php' );
    }
}