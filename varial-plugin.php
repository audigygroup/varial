<?php
/**
 * Varial: The base plugin to support common features and capabilities for Audigy and Stratus WordPress Sites
 *
 * 1. Universal Google Analytics Setting
 * 2. GA Pinger Script
 * 3. CallRail Setting
 *
 * @package   Varial_Plugin
 * @author    David Wright <dwright@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 *
 * @wordpress-plugin
 * Plugin Name:       Varial Plugin
 * Plugin URI:        @TODO
 * Description:       Varial: The base plugin to support common features and capabilities for Audigy and Stratus WordPress Sites
 * Version:           1.0.1
 * Author:            David Wright
 * Author URI:        @TODO
 * GitHub Plugin URI: git clone https://agwebdev@bitbucket.org/audigygroup/varial.git
 * WordPress-Plugin-Boilerplate: v1.0.0
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/*----------------------------------------------------------------------------*
 * Main Plugin -  Public-Facing Functionality
 *----------------------------------------------------------------------------*/

/*
 *
 */
require_once( plugin_dir_path( __FILE__ ) . 'public/class.VarialPlugin.php' );




/*
 * Register hooks that are fired when the plugin is activated or deactivated.
 * When the plugin is deleted, the uninstall.php file is loaded.
 *
 */
register_activation_hook( __FILE__, array( 'VarialPlugin', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'VarialPlugin', 'deactivate' ) );

/*
 *
 */
add_action( 'plugins_loaded', array( 'VarialPlugin', 'get_instance' ) );

/*----------------------------------------------------------------------------*
 * Main Plugin - Dashboard and Administrative Functionality
 *----------------------------------------------------------------------------*/

/*
 * If you want to include Ajax within the dashboard, change the following
 * conditional to:
 *
 * if ( is_admin() ) {
 *   ...
 * }
 *
 * The code below is intended to to give the lightest footprint possible.
 */
if ( is_admin()) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class.VarialPluginAdmin.php' );
	add_action( 'plugins_loaded', array( 'VarialPluginAdmin', 'get_instance' ) );
}


/*----------------------------------------------------------------------------*
 * Features - Admin and Public
 * Include the plugin controller
 *----------------------------------------------------------------------------*/

$options = get_option('features_enabled');
$options = json_decode($options);

if( !empty ($options->ga) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'features/google_analytics/google-analytics-plugin.php');
}
if( !empty ($options->callrail) && !empty($options->ga) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'features/callrail/callrail-plugin.php');
}

if( !empty ($options->faq) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'features/faq/faq-plugin.php');
}

if( !empty ($options->streamer_shortcode) ) {
	require_once( plugin_dir_path( __FILE__ ) . 'features/streamers-shortcode/streamers-shortcode.php' );
}

require_once( plugin_dir_path( __FILE__ ) . 'features/global-custom-scripts-option/global-custom-scripts-option.php');
require_once( plugin_dir_path( __FILE__ ) . 'features/per-page-custom-scripts-option/per-page-custom-scripts-option.php');