<?php

/**
 * Created by PhpStorm.
 * User: dwright
 * Date: 1/6/15
 * Time: 1:17 PM
 */
class VarialDashboardWidget {

	private function __construct() {
	}

	public function getInstance() {
		return new VarialDashboardWidget();
	}

	public function add_dashboard_widget() {
		wp_add_dashboard_widget( 'dashboard_widget', 'Example Dashboard Widget',
			array( $this, 'dashboard_widget_function' ) );

	}

	public function dashboard_widget_function() {

		if (is_plugin_active('wp-mail-smtp/wp_mail_smtp.php')) {
			echo "WP Mail SMTP ACTIVE!!! :-)";

		} else {
			echo "WP Mail SMTP NOT Active";
		}
	}
}