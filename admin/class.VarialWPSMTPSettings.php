<?php

/**
 * Class VarialWPSMTPSettings
 *
 * Force the SMTP configuration to always be set in Admin
 * And update all gravity forms notifications
 *
 * Currently runs on every admin page load
 */

class VarialWPSMTPSettings {

	const PLUGIN = 'wp-mail-smtp/wp_mail_smtp.php';

	/**
	 * wp_options set by WP_MAIL_SMTP plugin
	 */
	const SMTP_PASS = '$TwB38mV';
	const SMTP_USER = 'noreply';
	const SMTP_AUTH = 'true';
	const SMTP_SSL = 'tls';
	const SMTP_PORT = 25;
	const SMTP_HOST = 'mail.audigygroup.com';
	const MAIL_SET_RETURN_PATH = 'true';
	const MAILER = 'smtp';
	const MAIL_FROM_NAME = "";
	const MAIL_FROM = 'noreply@audigygroup.com';

	private function __construct() {

	}

	public function getInstance()
	{
		return new VarialWPSMTPSettings();
	}

	public function configure_smtp() {
		$this->activate_plugin();
	}

	protected function activate_plugin() {
		if (is_plugin_active(VarialWPSMTPSettings::PLUGIN)) {
			$this->set_wp_options();
			$this->update_gravity_forms_notifications();
		} else {
			$result = activate_plugin(VarialWPSMTPSettings::PLUGIN);
			/*
			if (is_wp_error( $result )) {
				echo "<pre>"; print_r($result); echo "</pre";
			}
			*/
		}
	}

	protected function set_wp_options() {
		update_option('smtp_pass', VarialWPSMTPSettings::SMTP_PASS);
		update_option('smtp_user', VarialWPSMTPSettings::SMTP_USER);
		update_option('smtp_auth', VarialWPSMTPSettings::SMTP_AUTH);
		update_option('smtp_ssl', VarialWPSMTPSettings::SMTP_SSL);
		update_option('smtp_port', VarialWPSMTPSettings::SMTP_PORT);
		update_option('smtp_host', VarialWPSMTPSettings::SMTP_HOST);
		update_option('mail_set_return_path', VarialWPSMTPSettings::MAIL_SET_RETURN_PATH);
		update_option('mailer', VarialWPSMTPSettings::MAILER);
		update_option('mail_from_name', VarialWPSMTPSettings::MAIL_FROM_NAME);
		update_option('mail_from', VarialWPSMTPSettings::MAIL_FROM);
	}


	protected function isJSON($string){
		return is_string($string) && is_object(json_decode($string)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
	}

	/**
	 * Update the from email address for each notification for each form.
	 * Gravity Forms stores this data in json or serialized array.
	 */
	protected function update_gravity_forms_notifications()
	{
		global $wpdb;

		$rs = $wpdb->get_results("SELECT form_id, notifications FROM wp_rg_form_meta", ARRAY_A);

		#echo "<pre>"; print_r($rs); echo "</pre>";

		if (!empty($rs)) {
			foreach ($rs as $k => $v) {

				#echo "Processing form id = " . $v['form_id'];

				$currentNotification = $v['notifications'];

				#echo "<pre>"; print_r($currentNotification); echo "</pre>";

				if ($this->isJSON($currentNotification)) {
					#echo "<pre>"; echo "json"; echo "</pre>";
					$notifications = json_decode($currentNotification, ARRAY_A);
				} else {
					#echo "<pre>"; echo "not json"; echo "</pre>";
					$notifications = unserialize($currentNotification);
				}

				#echo "<pre>Notifications:"; print_r($notifications); echo "</pre>";

				if (is_array($notifications) && !empty($notifications)) {
					$keys = array_keys( $notifications );

					#echo "<pre>"; print_r($keys); echo "</pre>";

					if ( ! empty( $keys ) ) {

						foreach ( $keys as $key ) {
							$notifications[ $key ]['from'] = VarialWPSMTPSettings::MAIL_FROM;
						}
						#echo "<pre>"; print_r($notifications); echo "</pre>";

						$nn = json_encode( $notifications );

						#echo "<pre>"; print_r($v['form_id']); echo "</pre>";

						$wpdb->update(
							'wp_rg_form_meta',
							array(
								'notifications' => $nn
							),
							array( 'form_id' => $v['form_id'] )
						);
					}
				}
			}
		}
	}
}