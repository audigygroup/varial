<?php
/**
* NOTE:    This is the view for Varial Controls on the admin side.
*
**/
?>
<div class="wrap">
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
	<form method="POST" action="options.php">
		<?php settings_fields( 'varial-controls' );
		do_settings_sections( 'varial-controls' );
		submit_button();
		?>
	</form>
</div>
