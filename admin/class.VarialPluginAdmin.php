<?php
/**
 * Varial_Plugin.
 *
 * @package   Varial_Plugin
 * @author    David Wright <dwright@audigygroup.com>
 * @copyright 2014 Audigy Group LLC
 */

/**
 * Plugin class. This class should ideally be used to work with the
 * administrative side of the WordPress site.
 *
 * If you're interested in introducing public-facing
 * functionality, then refer to `class.VarialPlugin.php`
 *
 * @package Varial_Plugin
 * @author    David Wright <dwright@audigygroup.com>
 */
require_once WP_PLUGIN_DIR .'/varial/includes/class.VarialHelpTab.php';
require_once WP_PLUGIN_DIR .'/varial/includes/class.FeatureAdminSwitch.php';

//require_once WP_PLUGIN_DIR .'/varial/admin/class.VarialDashboardWidget.php';
require_once WP_PLUGIN_DIR .'/varial/admin/class.VarialWPSMTPSettings.php';

class VarialPluginAdmin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_screen_hook_suffix = 'varial';

	/**
	 * Initialize the plugin by loading admin scripts & styles and adding a
	 * settings page and menu.
	 *
	 * @since     1.0.0
	 */
	private function __construct() {

		/*
		 * @TODO :
		 *
		 * - Uncomment following lines if the admin class should only be available for super admins
		 */
		/* if( ! is_super_admin() ) {
			return;
		} */

		/*
		 * Call $plugin_slug from public plugin class.
		 *
		 */
		$plugin = VarialPlugin::get_instance();
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Add the options page and menu item.
		add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Add an action link pointing to the options page.
		$plugin_basename = plugin_basename( plugin_dir_path( realpath( dirname( __FILE__ ) ) ) . $this->plugin_slug . '.php' );
		add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'add_action_links' ) );


		VarialHelpTab::getInstance();
		add_action( 'admin_menu', array($this, 'varialControlsMenu') );
		FeatureAdminSwitch::getInstance();

		//$varialDashboardWidget = VarialDashboardWidget::getInstance();
		//add_action('wp_dashboard_setup', array($varialDashboardWidget, 'add_dashboard_widget'));

		$varialWPSMTPSettings = VarialWPSMTPSettings::getInstance();
		add_action('admin_head', array($varialWPSMTPSettings, 'configure_smtp'));
	}

    /**
     * Return an instance of this class.
     *
     * @since     1.0.0
     *
     * @return    object    A single instance of this class.
     */
    public static function get_instance() {

        /*
         * @TODO :
         *
         * - Uncomment following lines if the admin class should only be available for super admins
         */
        /* if( ! is_super_admin() ) {
            return;
        } */

        // If the single instance hasn't been set, set it now.
        if ( null == self::$instance ) {
            self::$instance = new self;
        }

        return self::$instance;
    }


	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {
		wp_register_style( 'enqueue_admin_styles', plugins_url( 'varial/assets/varial-admin.min.css'), false );
		wp_enqueue_style( 'enqueue_admin_styles' );
	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since     1.0.0
	 *
	 * @return    null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {
        wp_enqueue_script( $this->plugin_slug . '-plugin-script', plugins_url('varial/assets/varial-admin.min.js' . realpath(__FILE__ . '../') ),array( 'jquery' ), '1.0.0', true);
	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		/*
		 * Add a settings page for this plugin to the Settings menu.
		 *
		 * NOTE:  Alternative menu locations are available via WordPress administration menu functions.
		 *
		 *        Administration Menus: http://codex.wordpress.org/Administration_Menus
		 *
		 * @TODO:
		 *
		 * - Change 'Page Title' to the title of your plugin admin page
		 * - Change 'Menu Text' to the text for menu item for the plugin settings page
		 * - Change 'manage_options' to the capability you see fit
		 *   For reference: http://codex.wordpress.org/Roles_and_Capabilities

		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Varial Plugin', $this->plugin_slug ),
			__( 'Varial Plugin', $this->plugin_slug ),
			'manage_options',
			$this->plugin_slug,
			array( $this, 'display_plugin_admin_page' )
		);
        */

		/*
		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'Google Analytics', 'googleanalytics' ),
			__( 'Google Analytics', 'googleanalytics' ),
			'manage_options',
			'googleanalytics',
			array( $this, 'display_ga_code_plugin_admin_page' )
		);


		$this->plugin_screen_hook_suffix = add_options_page(
			__( 'CallRail', 'callrail' ),
			__( 'CallRail', 'callrail' ),
			'manage_options',
			'callrail',
			array( $this, 'display_callrail_plugin_admin_page' )
		);
		*/

	}

	public function display_callrail_plugin_admin_page()
	{
		include_once('views/admin-callrail.php');
	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {
		include_once('views/admin.php');
	}

	public function display_ga_code_plugin_admin_page() {
		include_once('views/admin-google-analytics.php');
	}


	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'options-general.php?page=' . $this->plugin_slug ) . '">' . __( 'Settings', $this->plugin_slug ) . '</a>'
			),
			$links
		);

	}

	/**
	 * NOTE:     Actions are points in the execution of a page or process
	 *           lifecycle that WordPress fires.
	 *
	 *           Actions:    http://codex.wordpress.org/Plugin_API#Actions
	 *           Reference:  http://codex.wordpress.org/Plugin_API/Action_Reference
	 *
	 * @since    1.0.0
	 */
	public function action_method_name() {
		// @TODO: Define your action hook callback here
	}

	/**
	 * NOTE:     Filters are points of execution in which WordPress modifies data
	 *           before saving it or sending it to the browser.
	 *
	 *           Filters: http://codex.wordpress.org/Plugin_API#Filters
	 *           Reference:  http://codex.wordpress.org/Plugin_API/Filter_Reference
	 *
	 * @since    1.0.0
	 */
	public function filter_method_name() {
		// @TODO: Define your filter hook callback here
	}

	/**
	* NOTE:    This Method adds menu item in admin. Varial Controls
	*
	**/
	public function varialControlsMenu(){
	    add_menu_page( 'Varial Controls', 'Varial Controls', 'manage_options', 'varial-controls', array($this, 'varialConstrolsPage'), '' ); 
	}

	/**
	* NOTE:    This method includes the view for Varial Controls Admin page.
	*
	**/
	public function varialConstrolsPage(){
		include_once('views/featuresControlView.php');
	}

}
