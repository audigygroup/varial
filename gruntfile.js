'use strict';
module.exports = function(grunt) {

  grunt.initConfig({
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: [
        'Gruntfile.js',
        'assets/js/*.js',
        '!assets/js/varial.min.js'
      ]
    },
    less: {
        style: {
            options: {
                cleancss: true,
            },
            files: {
                'assets/varial.min.css': 'assets/less/varial.less',
                'assets/varial-admin.min.css': 'assets/less/varial-admin.less'
            }
        }
    },
    uglify: {
      dist: {
        files: {
          'assets/varial.min.js': [
            'features/google_analytics/public/assets/js/pinger.js',
            'features/faq/public/assets/js/accordion.js'
          ],
          'assets/varial-admin.min.js': [
          'features/faq/admin/assets/js/reorder.js'
          ]
        }
      }
    },
    version: {
      options: {
        file: 'lib/scripts.php',
        css: 'assets/varial.min.css',
        cssHandle: 'roots_main',
        js: 'assets/varial.min.js',
        jsHandle: 'roots_scripts'
      }
    },
    watch: {
      js: {
        files: [
          '<%= jshint.all %>'
        ],
        tasks: ['jshint', 'uglify', 'version']
      },
    },
    clean: {
      dist: [
        'assets/varial.min.css',
        'assets/varial.min.js'
      ]
    }
  });

  // Load tasks
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-notify');

  // Register tasks
  grunt.registerTask('default', [
    'clean',
    'less',
    'uglify'
  ]);
  grunt.registerTask('dev', [
    'watch'
  ]);

};